<?php declare(strict_types = 1);

namespace AjaxNette\Authorizator;

use AjaxNette\Authorizator\Security\Voter\IVoter;
use AjaxNette\Authorizator\Security\Voter\Voter;
use Nette\DI\Container;

/**
 * Class Authorizator
 *
 * @package AjaxNette\Authorizator
 */
class Authorizator
{
    /**
     * @var \Nette\Security\User
     */
    private $securityUser;
    /**
     * @var \Nette\DI\Container
     */
    private $container;

    public function __construct(\Nette\Security\User $securityUser, Container $container)
    {
        $this->securityUser = $securityUser;
        $this->container = $container;
    }

    /**
     * Can a user access resource?
     *
     * @param  string  $attribute
     * @param  mixed $subject
     *
     * @return bool
     */
    public function isGranted(string $attribute, $subject): bool
    {
        return $this->callVoter($attribute, $subject);
    }

    /***************************************************************************************************************
     * Helpers
     ***************************************************************************************************************/


    /**
     * Call a right voter class if exists. If does not exists return always false
     *
     * @param  string|  $attributes
     * @param $subject
     *
     * @return bool
     */
    private function callVoter(string $attribute, $subject): bool
    {
        $servicesNames = $this->container->findByTag('ajaxNette.authorizator.voter');
        /** @var \AjaxNette\Authorizator\Security\Voter\IVoter[] $services */
        $voters = [];
        /** @var int $vote */
        $vote = Voter::ACCESS_ABSTAIN;

        //load voters classes/objects from DI container
        foreach ($servicesNames as $servicesName => $value) {
            $voters[$servicesName] = $this->container->getService($servicesName);
        }

        //iterate over all existing voter clasess
        foreach ($voters as $key => $voter) {

            if(! $voter instanceof IVoter) {
                throw new InvalidArgumentException(
                    sprintf("Service '%s' does not implement '%s' interface", get_class($voter), IVoter::class)
                );
            }

            /** @var string[] $attributes */
            $attributes = (array) $attribute;

            //result
            $vote = $voter->vote($this->securityUser, $subject, $attributes);

            //allow
            if($vote > 0) {
                return true;
            }
        }

        //deny
        if ($vote <= 0) {
            return false;
        }
    }


}
