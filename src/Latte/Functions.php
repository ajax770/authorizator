<?php declare(strict_types = 1);

namespace AjaxNette\Authorizator\Latte;

use AjaxNette\Authorizator\Authorizator;
use Latte\Engine;

/**
 * Class Functions
 *
 * @package AjaxNette\Authorizator\Latte
 */
class Functions
{
    /** @var \Latte\Engine */
    private $latteEngine;
    /**
     * @var \AjaxNette\Authorizator\Authorizator
     */
    private $authorizator;

    public function __construct(Authorizator $authorizator)
    {
        $this->authorizator = $authorizator;
    }

    /**
     * @param \Latte\Engine $latteEngine
     * @return \Callback
     */
    public function registerFunctions(Engine $latteEngine)
    {
        $this->latteEngine = $latteEngine;
        $this->callback();

        return [$this, 'callback'];
    }

    public function callback()
    {
        $latteEngine = $this->latteEngine;

        $latteEngine->addFunction('isGranted', [$this->authorizator, 'isGranted']);
    }
}
