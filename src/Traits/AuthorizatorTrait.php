<?php declare(strict_types = 1);

namespace AjaxNette\Authorizator\Traits;

use AjaxNette\Authorizator\Authorizator;

/**
 * Trait AuthorizatorTrait
 *
 * @package AjaxNette\Authorizator\Traits
 */
trait AuthorizatorTrait
{
    /**
     * @var \AjaxNette\Authorizator\Authorizator
     */
    private $authorizator;

    public function __construct(Authorizator $authorizator)
    {
        $this->authorizator = $authorizator;
    }

    /**
     * Can a user access resource?
     *
     * @param  string  $attribute
     * @param  mixed $subject
     *
     * @return bool
     */
    public function isGranted($attribute, $subject): bool
    {
        return $this->authorizator->isGranted($attribute, $subject);
    }
}